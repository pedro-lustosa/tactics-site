'use strict';

( function () {
  // Identificadores
  var summary = document.getElementById( 'manual-sections-list' ),
      summaryMarkers = Array.from( summary.getElementsByClassName( 'marker' ) ),
      markersLists = summaryMarkers.map( marker => marker.parentElement.querySelector( '.marker + ol' ) );

  // Eventos

  /// Alterna expansão de listas do sumário ao se clicar em seus marcadores
  for( let marker of summaryMarkers ) marker.addEventListener( 'click', toggleList );

  // Funções

  /// Alterna expansão de listas do sumário
  function toggleList( event ) {
    // Identificadores
    var marker = event.currentTarget,
        list = markersLists[ summaryMarkers.indexOf( marker ) ];

    // Redefine estilo de linha da altura atual da lista
    list.style.height = list.offsetHeight + 'px';

    // Força computação do estilo de linha redefinido, para que transição de altura seja animada
    void list.offsetHeight;

    // Alterna atividade do marcador e o tamanho da lista
    list.style.height = marker.classList.toggle( 'active' ) ? ( list.scrollHeight + 'px' ) : '0';

    // Itera por listas ascendentes
    for( let targetList = list.parentElement.closest( 'ol' ); targetList != summary; targetList = targetList.parentElement.closest( 'ol' ) ) {
      // Atualiza tamanho da lista ascendente
      targetList.style.height = 'auto';
    }
  }
} )();
